# Bluetooth starter project on a Raspberry Pi

So - I got a lovely new Raspberry Pi 4B - along with an AlphaBot2 robot kit from Waveshare.

I wanted to just drive it around with a Bluetooth app on my phone - but using C++ instead of Python.

>*This turned out to be something really hard to get done.*

Long story short - There's enough documentation - but it doesn't make sense if you're learning about Bluetooth for the first time.

So this code shows how to respond to Bluetooth signals using C++ code.

---

### The learning curve

So - it turns out that the standard Bluetooth libraries on the Pi work in an non-intuitive way.

Also - if one is learning C++ for the first time - there's the whole pointer and object syntax that needs to be understood.

To cut a long story short - there is documentation on how to use the **libbluetooth1-dev** libraries...
...but it doesn't show how the objects should be used together to get a Bluetooth connection up and running.

There are also *some* examples for C++ coding - but they all block the program from exiting when an interruption happens.

---

### My solution

So - this project is to help any other person trying to get Bluetooth to work on Linux with C++

It does the following: -

1. Loads up a process that listens for Bluetooth connections.
(you need to get your phone and your bot to pair and trust each other before running the program).
2. when a connection is detected - the console shows the MAC address of the phone / remote that's now connected.
3. when you press on controls - the console shows the text string sent to the bot through the Bluetooth link.
4. All the code is non-blocking - so at any point you can stop the program without an error.
5. The code is thread safe - so the Bluetooth monitoring can happen in it's own thread - and not clutter up your main project's code.


---

### Gettin the project up and running.

Just some quick notes on the code -
> 1. The project is built using **Code::Blocks**
(this is because I'm learning the C++ language - and this IDE has intellisense and other nice-to-have features that are helping me get over the learning curve).
>
>2. There's a **makefile** as well - so the regular **make** command will work too.

Now - if you're starting out with a raspberry pi OS from 2020 or later, then there are some steps you need to do before this code will work.

#### Preparing the Pi for the code to work: -

run this command to install the Bluetooth developers' library: -

**sudo apt install libbluetooth-dev**

Next to get a Bluetooth connection up and running.

>These steps are a summary of the instructions in [This AlphaBot Manual](https://www.mouser.com/pdfdocs/Alphabot2-user-manual-en.pdf)
>(steps 4 and 5 on pages 43 and 44).

2. you need to edit the Bluetooth config settings.
run:
    **sudo nano /etc/systemd/system/dbus-org.bluez.service**
    
	    This opens up a text editor. You need to change the row that reads
    		ExecStart=/usr/lib/bluetooth/bluetoothd
    	to 2 rows tha look like this: -
    		ExecStart=/usr/lib/bluetooth/bluetoothd -C
    		ExecStartPost=/usr/bin/sdptool add SP

3. reboot for the changes to take effect:
	**sudo reboot**

4. now - check if bt is working:
	**sudo hciconfig**
	(if working you'll see the hci0 device)

5. Finally - pair your phone to the AlphaBot2

This is done using the **bluetoothctl** command from the command shell.
start by running 
	**sudo bluetoothctl**

you'll get the response 

	Agent registered
	[bluetooth]#
then type in the commands as shown in bold - the responses I got are shown as code blocks.
**agent on**
	
	[bluetooth]# agent on
	Agent is already registered
**default-agent**

	[bluetooth]# default-agent
	Default agent request successful
**scan on**

	[bluetooth]# scan on
	Discovery started
	[CHG] Controller DC:A6:32:21:94:35 Discovering: yes
	[NEW] Device EC:56:23:BE:45:80 David's P30 Pro
**pair EC:56:23:BE:45:80**

	[bluetooth]# pair EC:56:23:BE:45:80
	Attempting to pair with EC:56:23:BE:45:80
	[CHG] Device EC:56:23:BE:45:80 Connected: yes
	Request confirmation
	[agent] Confirm passkey 654051 (yes/no):
**yes**

	[agent] Confirm passkey 654051 (yes/no): yes
	[CHG] Device EC:56:23:BE:45:80 Modalias: bluetooth:v010Fp107Ed1436
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 0000046a-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001105-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 0000110a-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 0000110c-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001112-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001115-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001116-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 0000111f-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 0000112f-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001132-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001200-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001800-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 00001801-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 UUIDs: 0000fe35-0000-1000-8000-00805f9b34fb
	[CHG] Device EC:56:23:BE:45:80 ServicesResolved: yes
	[CHG] Device EC:56:23:BE:45:80 Paired: yes
	Pairing successful
	[CHG] Device EC:56:23:BE:45:80 ServicesResolved: no
	[CHG] Device EC:56:23:BE:45:80 Connected: no
	[bluetooth]#
**exit**

Now you should be back on the Linux command prompt - and the Raspberry is configured so that the program will work.

### Running and testing the code. 
The Raspberry Pi should now have Bluetooth configured and it should accept connections from your phone.

It is finally time to run the code.

So - to run the code: -

1. Clone this project - or use any other method to get the code onto the raspberry pi .
2. run the make command:
**make**
3. now you can run the code:
**./bin/Release/non-bot-test**

That's it. Now the console will show when you've connected to the bot from your phone.

> I tested with an android phone - using an app called "Bluetooth Electronics" by keuwlsoft.
>
> When you send a signal to the bot - then you should see the signal shown in the console.

To end the program - press break (or **ctrl-c**).

