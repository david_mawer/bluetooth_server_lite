#include <iostream>   // for standard I/O in C++
#include <cstdio>     // for printf
#include <cstdlib>	  // for exit() definition
#include <cmath>	  // for mathematical funtions
#include <thread>     // for threading out loud
#include <csignal>	  // for catching exceptions e.g. control-C
#include <unistd.h>	  // this one is to make usleep() work
#include <string>
#include <atomic>
#include <list>
#include <bt_handler.h>

/* This global variable is used to interrupt the infite while loops with signaction()
 this is important as if not used and tryiong to stop the program with Ctrl-c,
 the program might go full speed against the  wall (it happened) */
std::atomic<int> interrupted;
// Variable to take care of threading.
std::list<std::thread> threadList;

void joinThreads() {
    // join all the threads to this main one (required to finish off cleanly).
    for (std::thread &myThread : threadList)
    {
        if (myThread.joinable()) {
            myThread.join();
        }
    }
}

/* This is the signal() function handler;
 *  Used to end the program cleanly.
 */
void signal_callback_handler(int signum) {
    printf("Caught signal %d, shutting down...\n", signum);
    interrupted = 1;
    joinThreads();
    exit(1);
}


// this is the callback for the bluetooth handler.
//  the string is the value received from the bluetooth client.
void onBTInput(std::string psBuff) {

    // show it on the console.
    std::cout << psBuff << "\n";

}

// fires up the bluetooth handler in it's own thread.
void runBlueTooth() {
    bt_handler myBT(onBTInput);
    myBT.run(std::ref(interrupted));
    return;
}

int main(int argc, char * argv[]) {
    // Handle interruptions gracefully (user presses break)
    signal(SIGINT, signal_callback_handler);

    // set up the bluetooth handler on a seperate thread.
    threadList.push_back(std::thread(runBlueTooth));

    // run the main thread - for this demo it does nothing.
    while (interrupted == 0) {
        usleep(10000);
    }


    // Close down nicely.
    joinThreads();
    exit(0);
}
