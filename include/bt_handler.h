#ifndef BT_HANDLER_H
#define BT_HANDLER_H
#include <atomic>
#include <functional>
#include <vector>

class bt_handler
{
    using TCallback = std::function<void(const std::string)>;

    public:
        bt_handler(TCallback);
        virtual ~bt_handler();
        void run(std::atomic<int>&);

    protected:

    private:
        std::string convertToString(char* a);
};

#endif // BT_HANDLER_H
